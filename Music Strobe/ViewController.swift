//
//  ViewController.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 8/07/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds
import AVKit

class ViewController: UIViewController  {
    
    @IBOutlet weak var volLevel: UILabel!
    @IBOutlet weak var rmsLab: UILabel!
    @IBOutlet weak var meanLab: UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet var d:DragRotateGestureRecognizer!
    @IBOutlet var tapRecognizer: UITapGestureRecognizer!
    @IBOutlet var touchDownRecognizer: TouchDownGestureRecognizer!
    @IBOutlet weak var colorArrow: UIImageView!
    
    @IBOutlet weak var buttonOuterView: UIImageView!
    @IBOutlet weak var innerBtnView: UIImageView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var screenToggleBtn: UIBarButtonItem!
    @IBOutlet var ledFlashBtn: UIBarButtonItem!
    
    @IBOutlet weak var flasherView: FlasherView!
    
    var proModeEnabled = true
    
    let screenBtnUpImg = UIImage(named: "screenonbtnup")
    var rotatingBtn:RotatingButton!
    var fftFlasher:FFTFlasher!
    var appRuns = 0;

    //toggle between showing and hidding the graph function
    var graphHidden:Bool = true
    
    /** super powered stuff **/
    var superpowered:Superpowered!
    var displayLink:CADisplayLink!
    var layers:[CALayer]!
    var meanLayers:[CALayer]!
    var silenceLayers:[CALayer]!
    //number of frequincies
    let f = 8
    var fSliders = [UISlider]()
    var defaults = UserDefaults.standard
    

    override func viewDidLoad() {
        super.viewDidLoad()

        //want to count how many times the app has been loaded to show rating popup
        appRuns = defaults.integer(forKey: "runs")
        
        bannerView.adSize = GADLandscapeAnchoredAdaptiveBannerAdSizeWithWidth(UIScreen.main.bounds.size.width)
        
#if DEBUG
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
#else
        bannerView.adUnitID = "ca-app-pub-1933636331544287/3084602418"
#endif
        bannerView.rootViewController = self
        bannerView.load(GAMRequest())
        bannerView.delegate = self
        
        
        fftFlasher = FFTFlasher(flasherView: flasherView , numFrequencies: f,ledFlashBtn: ledFlashBtn)
        
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "musicstrobetype"))
        self.view.tintColor = UIColor(red:1.0/255.0 , green: 5.0/255.0 , blue: 5.0/255.0, alpha: 1.0)
        
        if(!fftFlasher.setupDone){
            fftFlasher.loadStoredDefaultValues()
            fftFlasher.setupBuffer()
            fftFlasher.setupTorch()
        }
        
        setupGestureRecognizers()
        
        setupSliders()
        
        superpowered = Superpowered()
        
        // A display link calls us on every frame (60 fps).
        displayLink = CADisplayLink(target: self, selector: #selector(ViewController.onDisplayLink))
        displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        /** end of superpowererd stuff**/
    }
    
    /* check if flash or screen is on and perform actions
     *this function is called when toglling flash and screen buttons
     */
    func checkRunning(){
        if(fftFlasher.screenOn||fftFlasher.flashOn){
            //if on keep awake
            //    NSLog("idletimerdisabled  = true")
            UIApplication.shared.isIdleTimerDisabled = true
            //increment counter for checking if to show rating screen
            
            appRuns = appRuns + 1;
            //print(" appruns" + String(appRuns));
            defaults.set(appRuns, forKey: "runs")
            if(appRuns == 7 || appRuns == 50){
                SKStoreReviewController.requestReview();
            }
        }
        else{
            //if nothing on then let sleep
            //  NSLog("idletimerdisabled false")
            UIApplication.shared.isIdleTimerDisabled = false
        }
    }
    
    @IBAction func toggleFlashAction(_ sender: UIBarButtonItem) {
        
        if(fftFlasher.flashOn){
            sender.image = UIImage(named: "flashOffBtnImg")
            fftFlasher.flashOn = false
            
            // clear graph bars incase youre in graphmode without anything on
            for n in 0...f-1 {
                meanLayers[n].frame.size.height = 0
                silenceLayers[n].frame.size.height = 0
                layers[n].frame.size.height = 0
            }
        }
        else{
            sender.image = UIImage(named: "flashOnBtnImg")
            fftFlasher.flashOn = true
        }
        checkRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //   NSLog(" view will appear")
        let width:CGFloat = (self.view.frame.size.width) / CGFloat(f)
        let yPos =  view.frame.height * 0.2
        let height = view.frame.height * 0.7
        
        for n in 0...f-1{
            //Now that we have a frame, set the slider frame
            
            let sliderFrame = CGRect(x: width*CGFloat(n), y: yPos, width: width, height: height)
            fSliders[n].frame = sliderFrame
            if(graphHidden){
                fSliders[n].isHidden = true
            }
            else{
                fSliders[n].isHidden = false
            }
        }
    }
    
    
    //This is called after the rotation, thus it has the correct size
    override func viewWillLayoutSubviews() {
        //TODO fix height
        let width:CGFloat = (self.view.frame.size.width) / CGFloat(f)
        let yPos =  view.frame.height * 0.2
        let height = view.frame.height * 0.7
        
        for n in 0...f-1{
            //Now that we have a frame, set the slider frame
            let sliderFrame = CGRect(x: width*CGFloat(n), y: yPos, width: width, height: height)
            fSliders[n].frame = sliderFrame
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToMainView(_ sender: UIStoryboardSegue){
        fftFlasher.loadStoredDefaultValues()
        fftFlasher.setupBuffer()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //about to go to settings so stop running
        
    }
    
    override var prefersStatusBarHidden : Bool {
        
        if fftFlasher != nil{
            if(!fftFlasher.screenOn){
                return false
            }
        }
        
        if self.navigationController?.isNavigationBarHidden == true {
            
            if buttonView != nil{
                buttonView.isHidden = true
            }
            return true
            
        } else {
            // if have a buttonView (wont have at start up and we are not on the graphView then can show button
            if buttonView != nil && graphHidden == true{
                buttonView.isHidden = false
            }
            return false
        }
    }
}





