//
//  FlasherView.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 2/11/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import UIKit

//@IBDesignable makes it update the view in story board mode

@IBDesignable class FlasherView: UIView {
    
    var screenOff = true
    var alpha0 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    var color = UIColor.white
    var darken = false
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        if(screenOff){
            alpha0.setFill()
        }
        else{
            if(darken){
            color.withAlphaComponent(0.2).setFill()
            }
            else{
                color.setFill()
            }
        }
        UIRectFill(rect)
        
    }
    
    func off(){
        screenOff = true
        self.setNeedsDisplay()
    }
    
    func on(){
        screenOff = false
        self.setNeedsDisplay()
    }
    
    func changeColor(_ color:UIColor){
        self.color = color
        self.setNeedsDisplay()
    }
    
    func darken(_ darken:Bool){
        self.darken = darken
    }
    
}


extension UIColor {
    
    class func randomColor() -> UIColor{
        
        let randomFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        
        let randomColor = UIColor(hue: randomFloat, saturation: 1, brightness: 1, alpha: 1)
        
        return randomColor
        
        
    }
    
}
