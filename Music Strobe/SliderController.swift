//
//  SliderController.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 7/02/22.
//  Copyright © 2022 Thomas Kiddle. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    
    func setupSliders(){
        /** superpowered stuff**/
        // Setup 8 layers for frequency bars.
        let color:CGColor = UIColor.init(white: 0.1, alpha: 1).cgColor
        let meanColor:CGColor = UIColor(red: 0.0, green: 1.0, blue: 0.9, alpha: 0.9).cgColor
        let silenceColor:CGColor = UIColor(red: 0.9, green: 0.0, blue: 0.9, alpha: 0.9).cgColor
        
        layers = [CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer()]
        meanLayers = [CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer()]
        silenceLayers = [CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer(), CALayer()]
        for n in 0...f-1 {
            layers[n].backgroundColor = color
            layers[n].frame = CGRect.zero
            self.flasherView.layer.addSublayer(layers[n])
            
            meanLayers[n].backgroundColor = meanColor
            meanLayers[n].frame = CGRect.zero
            self.flasherView.layer.addSublayer(meanLayers[n])
            
            silenceLayers[n].backgroundColor = silenceColor
            silenceLayers[n].frame = CGRect.zero
            self.flasherView.layer.addSublayer(silenceLayers[n])
            
            //create each slider
            //Initialize slider as 0, then update once we have the frame properties.
            fSliders.append( UISlider(frame: CGRect.zero))
            //Rotate to a vertical slider
            fSliders[n].transform = CGAffineTransform(rotationAngle: CGFloat(-Float.pi/2))
            fSliders[n].minimumValue = 0.1
            
            fSliders[n].value = fftFlasher.fSliderValues[n]
            //if this is 100 then can have backups of flashes because flashing too much
            fSliders[n].maximumValue = 0.99
            fSliders[n].addTarget(self, action: #selector(ViewController.sliderChanged(_:)), for: .valueChanged)
            fSliders[n].isContinuous = true
            fSliders[n].isHidden = true
            
            
            var sliderAlpha:CGFloat = 1.0;
            
            if(!proModeEnabled){
                fSliders[n].isEnabled = false
                sliderAlpha = 0.3
                
            }
            else{
                fSliders[n].isEnabled = true
                sliderAlpha = 1
            }
            
            //slider appearance
            fSliders[n].maximumTrackTintColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: sliderAlpha)
            //  fSliders[n].minimumTrackTintColor  =  UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: sliderAlpha)
            //overiding last line to chagne color of sliders
            setSliderColor(slider: fSliders[n], val: fftFlasher.fSliderValues[n])
            
            fSliders[n].thumbTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: sliderAlpha  )
            fSliders[n].setThumbImage(UIImage(named: "sliderImg"), for: UIControl.State())
            fSliders[n].setThumbImage(UIImage(named: "sliderImg"), for: UIControl.State.highlighted)
            view.addSubview(fSliders[n])
        }
        
        self.enableSliders()
        self.proModeEnabled = true
        
    }
    
    func saveSliderValues(){
        
        var tempSliderValues : [NSNumber] = [NSNumber]()
        for n in 0...f-1{
            tempSliderValues.append(fftFlasher.fSliderValues[n] as NSNumber)
        }
        
        let vals : NSArray = tempSliderValues as NSArray
        defaults.set(vals, forKey: "sliderValues")
    }
    
    
    func enableSliders(){
        
        for n in 0...f-1{
            
            fSliders[n].isEnabled = true
            
            //slider appearance
            //fSliders[n].minimumTrackTintColor  =  UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
            fSliders[n].maximumTrackTintColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            setSliderColor(slider: fSliders[n], val: fftFlasher.fSliderValues[n])
            
            fSliders[n].thumbTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1  )
            fSliders[n].setThumbImage(UIImage(named: "sliderImg"), for: UIControl.State())
            fSliders[n].setThumbImage(UIImage(named: "sliderImg"), for: UIControl.State.highlighted)
        }
    }
    
    func hideBars(){
        self.navigationController?.hidesBarsOnTap = true
        self.navigationController?.hidesBarsOnSwipe = true
    }
    func showBars(){
        self.navigationController?.hidesBarsOnTap = false
        self.navigationController?.hidesBarsOnSwipe = false
    }
    
    func setSliderColor(slider:UISlider, val:Float){
        
        var red : Float = 0
        var green : Float = 0
        var blue : Float = 0.2
        
        if(val > 0.72){
            red = (val - 0.72) * 3.125
            green = (1 - val) * 3.125
            
        }
        else if (val <= 0.72 && val >= 0.58){
            red = 0
            green = 1
        }
        else if(val < 0.58){
            red = (0.58 - val) * 0.2
            green = pow((val * 1.7241),2)
            blue = red
        }
        
        let scolor  = UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1)
        slider.tintColor = scolor
        slider.minimumTrackTintColor  = scolor
    }
    
    @objc func sliderChanged(_ sender: UISlider){
        for n in 0...f-1{
            if(fSliders[n]==sender){
                
                var val = sender.value
                //want to snap to center slightly
                //TODO: change this probably. dont really want to imply to have them at 50?
                if(val>0.52 && val<0.57){
                    val = 0.55
                }
                
                if(val>0.59 && val<0.65){
                    val = 0.62
                    sender.value = val
                }
                fftFlasher.fSliderValues[n] = val
                //  NSLog(" changed value" + String(fftFlasher.fSliderValues[n]) + " f" + String(n))
                
                setSliderColor(slider: sender, val: val)
            }
        }
        //TODO move this to exiting functions if slow
        saveSliderValues()
    }
    
    @IBAction func toggleGraph(_ sender: UIBarButtonItem) {
        
        if(!graphHidden){
            sender.image = UIImage(named: "graphOffBtnImg")
            //show colorpicker
            buttonView.isHidden = false
            
            //clear the bars
            for n in 0...f-1 {
                layers[n].backgroundColor = UIColor.clear.cgColor
                meanLayers[n].backgroundColor = UIColor.clear.cgColor
                silenceLayers[n].backgroundColor = UIColor.clear.cgColor
                fSliders[n].isHidden = true
            }
            graphHidden = true
            //when not on graph hide nav bar on tap if screen on
            if(fftFlasher.screenOn){
                self.navigationController?.hidesBarsOnTap = true
                self.navigationController?.hidesBarsOnSwipe = true
            }
            flasherView.darken(false)
        }
        else{
            sender.image = UIImage(named: "graphOnBtnImg")
            //hide color picker
            buttonView.isHidden = true
            // print(" hid color picker")
            
            //show bars back
            for n in 0...f-1 {
                let color:CGColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1).cgColor
                let meanColor:CGColor = UIColor(red: 0.0, green: 1.0, blue: 0.9, alpha: 0.9).cgColor
                let silenceColor:CGColor = UIColor(red: 0.9, green: 0.0, blue: 0.9, alpha: 0.9).cgColor
                layers[n].backgroundColor = color
                meanLayers[n].backgroundColor = meanColor
                silenceLayers[n].backgroundColor = silenceColor
                fSliders[n].isHidden = false
            }
            graphHidden = false
            //when on graph view stop hidding nav bar on swipe
            self.navigationController?.hidesBarsOnTap = false
            self.navigationController?.hidesBarsOnSwipe = false
            if(fftFlasher.screenOn){
                //  fftFlasher.toggleScreenFlash()
                //do this to update the color ring to current mode
                //  scaleDownColorRing()
                flasherView.darken(true)
            }
        }
    }
    
}
