//
//  RotatingButton.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 12/12/17.
//  Copyright © 2017 Thomas Kiddle. All rights reserved.
//

import Foundation
import UIKit

class RotatingButton : NSObject {
    
    
    var changed = false
    var lastXr = CGFloat(Float.pi)
    var totalAngleMoved: CGFloat = 0
    
    var scaleMatrix = CGAffineTransform(scaleX: 1.15, y: 1.15)
    //want to start of at white which is at 180 degrees/PI radians
    var rAngleLast:CGFloat = CGFloat(Float.pi)
    var angleDif :CGFloat = 0
    var xr : CGFloat = CGFloat(Float.pi)
    
    var innerBtnView:UIImageView
    var buttonOuterView:UIImageView
    var colorArrow:UIImageView
    var buttonView:UIView
    
    let innerCircleOnImg = UIImage(named: "innercirclebtn")
    let innerCircleOffImg = UIImage(named: "innerbtnoff")
    let innerCircleRandomImg = UIImage(named: "innerbtnrandom")
    
    
    let fftFlasher:FFTFlasher
    let flasherView:FlasherView
    
    let viewController:ViewController
   
    init(viewController:ViewController,innerBtnView:UIImageView,buttonOuterView:UIImageView,buttonView:UIView, fftFlasher:FFTFlasher,flasherView:FlasherView,colorArrow:UIImageView){
        self.innerBtnView = innerBtnView
        self.buttonOuterView = buttonOuterView
        self.buttonView = buttonView
        self.fftFlasher = fftFlasher
        self.flasherView = flasherView
        self.colorArrow = colorArrow
        self.viewController = viewController
    }
    
    func dragRotateAction(sender : DragRotateGestureRecognizer){
        
        if ( sender.state == .began){
            
            DispatchQueue.main.async {
                self.innerBtnView.image = self.innerCircleOnImg
                self.scaleUpColorRing()
                self.setButtonColor()
                
                self.angleDif =  (sender.rAngle - self.rAngleLast).mod2PI
            }
            
        }
        
        if(sender.state == .changed){
            DispatchQueue.main.async {
                
                self.changed = true
                self.xr = (sender.rAngle - self.angleDif).mod2PI
                self.xr = self.xr.truncatingRemainder(dividingBy: CGFloat(2.00 * Double.pi))
                
                //working out total angle movement if it jumps more than PI then we cross
                //the 0/360 edge and need to factor that in
                let angleMovedDif = self.lastXr - self.xr
                if(angleMovedDif > CGFloat(Float.pi)){
                    self.totalAngleMoved -= CGFloat(2 * Float.pi)
                }
                if (angleMovedDif < CGFloat(-Float.pi)){
                    self.totalAngleMoved += CGFloat(2 * Float.pi)
                    
                }
                
                self.totalAngleMoved += angleMovedDif
                self.lastXr = self.xr
                
                self.setButtonColor()
                
                self.buttonOuterView.transform = self.scaleMatrix.rotated(by: -self.xr)
                self.rAngleLast = sender.rAngle.mod2PI
                
            }
        }
        if (sender.state == .ended){
            
            DispatchQueue.main.async {
                self.scaleDownColorRing()
                //if weve changed the color ring then we need to update the last angle otehrwise leave it
                if(self.changed){
                    self.rAngleLast = (self.rAngleLast - self.angleDif).mod2PI
                    self.changed = false
                }
                self.totalAngleMoved = 0
            }
        }
        if ( sender.state == .cancelled){
            DispatchQueue.main.async {
                self.scaleDownColorRing()
            }
        }
    }
    
    /** this gets run evertime we touch the button and is where
     the screen off toggle is **/
    func scaleDownColorRing(){
        // create matrix of identity then rotate to current rotation
        var matrix = CGAffineTransform.identity
        matrix = matrix.rotated(by: -xr)
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIView.AnimationOptions.curveLinear,
                       animations: { self.buttonOuterView.transform = matrix},
                       completion: { finished in if (true) { } } )
        
        if(fftFlasher.screenOn){
            self.innerBtnView.image = self.innerCircleOnImg
            //want to reset the button color to the selected color
            self.setButtonColor()
            
            if(viewController.graphHidden){
                viewController.hideBars()
            }
        }
        else{
            self.innerBtnView.image = self.innerCircleOffImg
           viewController.showBars()
        }
        
        //hide the color arrow, this also hides while starting app gets run in didload
        colorArrow.isHidden = true
    }
    
    
    
    func setButtonColor(){
        
        //if degree is within white threshold 170 - 190 leave color as white
        var c = UIColor.white
        let tinted = self.innerCircleOnImg?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.innerBtnView.image = tinted
        
        let xd = self.xr.mod2PI.radiansToDegrees.truncatingRemainder(dividingBy: 360)
        if(xd < 192 && xd > 168){
            fftFlasher.isRandomColor = false
        }
            
        else{
            fftFlasher.isRandomColor = false
            let h = xd/360.0
            c = UIColor(hue: h, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        }
        
        self.innerBtnView.tintColor = c
        //if random overwrite all this stuff before and set image to the image
        if(abs(totalAngleMoved) > CGFloat(2 * Float.pi)){
            fftFlasher.isRandomColor = true
            innerBtnView.image = innerCircleRandomImg
        }
        self.flasherView.changeColor(c)
    }
    
    func scaleUpColorRing(){
        
        // create scaled matrix then rotate to current rotation
        
        var matrix = scaleMatrix
        
        //enlarge the color arrow using matrix before rotating it for colorwheel
        colorArrow.transform = matrix
        colorArrow.isHidden = false
        
        matrix = matrix.rotated(by: -xr)
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIView.AnimationOptions.curveLinear,
                       animations: { self.buttonOuterView.transform = matrix},
                       completion: { finished in if (true) { } } )

    }
}
