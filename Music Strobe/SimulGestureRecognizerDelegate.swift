//
//  SimulGestureRecognizerDelegate.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 29/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import UIKit


class SimulGestureRecognizerDelegate: NSObject, UIGestureRecognizerDelegate {

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
     //   NSLog(" grd should begin")
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
       //   NSLog(" grd should recog simul")
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
}
