//
//  DragRotateGestureRecognizer.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 13/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class DragRotateGestureRecognizer: UIGestureRecognizer {
    
    var initialPoint =  CGPoint()
    var rAngle  = CGFloat()
    var rInitialAngle = CGFloat()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if let touch = touches.first {
            initialPoint = touch.location(in: view)
            
            let center  = CGPoint(x: (view?.bounds)!.midX, y: (view?.bounds)!.midY)
            
            //have the relative point from centre of button
            let x =  initialPoint.x - (center.x)
            let y =  (center.y) - initialPoint.y
            rAngle = CGFloat(Float.pi) + atan2(y,x)
       
            state = .began
        }
        
        super.touchesBegan(touches, with:event)
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        // NSLog(String(touches.first))
        if let touch = touches.first {
            
            let center  = CGPoint(x: (view?.bounds)!.midX, y: (view?.bounds)!.midY)
            
            //have the relative point from centre of button
            let x =  touch.location(in: view).x - (center.x)
            let y =  (center.y) - touch.location(in: view).y
            
            rAngle = CGFloat(Float.pi) + atan2(y,x)

        }
    
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        
        state = .ended
    }
    
    
    
}

extension CGFloat {
        var degreesToRadians : CGFloat {
        return CGFloat(self) * CGFloat(Float.pi) / 180.0
    }
}

extension CGFloat {
        var radiansToDegrees : CGFloat {
        return CGFloat(self) * 180 / CGFloat(Float.pi)
    }
}
extension CGFloat {
        var mod2PI : CGFloat {
        
            
            
            
            return (self.truncatingRemainder(dividingBy: CGFloat(2.00 * Float.pi)) + CGFloat(2.00 * Float.pi) ).truncatingRemainder(dividingBy: CGFloat(2.00 * Float.pi))
            
            //swift 2.3 version
            //return (self % CGFloat(2.00 * Float.pi) + CGFloat(2.00 * Float.pi) ) % CGFloat(2.00 * Float.pi)
            
        
    }
}

