//
//  ViewControllerViews.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 7/02/22.
//  Copyright © 2022 Thomas Kiddle. All rights reserved.
//

import Foundation
import UIKit

extension ViewController:UIGestureRecognizerDelegate{
    
    @IBAction func dragRotateAction(_ sender : DragRotateGestureRecognizer){
        rotatingBtn.dragRotateAction(sender:sender)
    }
    
    ///toggle on off button
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        if(sender.state == .ended){
            
            fftFlasher.toggleScreenFlash()
            
            //reset graphbars incase you go to graph while not on
            for n in 0...f-1 {
                meanLayers[n].frame.size.height = 0
                silenceLayers[n].frame.size.height = 0
                layers[n].frame.size.height = 0
            }
            checkRunning()
        }
        
    }
    
    func setupGestureRecognizers(){
        //load gesturerecognizers
        tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapGestureAction(_:)))
        tapRecognizer.delegate = self
        buttonView.addGestureRecognizer(tapRecognizer)
        
        rotatingBtn = RotatingButton(viewController:self, innerBtnView:innerBtnView, buttonOuterView:buttonOuterView, buttonView:buttonView, fftFlasher:fftFlasher,flasherView:flasherView, colorArrow:colorArrow)
        //reload teh colour ring so it loads the xr value at white
        rotatingBtn.scaleDownColorRing()
        
        d = DragRotateGestureRecognizer(target: self, action: #selector(dragRotateAction(_:)))
        //  d.requireGestureRecognizerToFail(tapRecognizer)
        // d.requireGestureRecognizerToFail(touchDownRecognizer)
        d.delegate = self
        buttonView.addGestureRecognizer(d)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        var g1 = false
        var g2 = false
        if( gestureRecognizer == tapRecognizer || gestureRecognizer == d){
            g1 = true
        }
        
        if(otherGestureRecognizer == tapRecognizer || otherGestureRecognizer == d){
            g2 = true
        }
        
        return g1 && g2
    }
}
