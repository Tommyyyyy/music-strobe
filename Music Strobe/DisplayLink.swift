//
//  DisplayLink.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 7/02/22.
//  Copyright © 2022 Thomas Kiddle. All rights reserved.
//

import Foundation
import AVKit

extension ViewController{
    
    @objc func onDisplayLink() {
        
        // Get the frequency values.
        let frequencies = UnsafeMutablePointer<Float>.allocate(capacity: f)
        superpowered.getFrequencies(frequencies)
        
        fftFlasher.evaluate(frequencies)
        
        //if its NOT on graph mode then dont bother to adjust the bars
        if(!graphHidden ){
            // Wrapping the UI changes in a CATransaction block like this prevents animation/smoothing.
            CATransaction.begin()
            CATransaction.setAnimationDuration(0)
            CATransaction.setDisableActions(true)
            // Set the dimension of every frequency bar.
            let originY:CGFloat = self.view.frame.size.height
            let width:CGFloat = (self.view.frame.size.width) / CGFloat(f)
            var frame:CGRect = CGRect(x: 0, y: 0, width: width, height: 0)
            var meanFrame:CGRect = CGRect(x: 0, y: 0, width: width, height: 0)
            var silenceFrame:CGRect = CGRect(x: 0, y: 0, width: width, height: 0)
            //TODO: move as much stuff outa this function ie this stretch can be created outside this looping function
            
            for n in 0...f-1 {
                //for first graph
                
                //for first graph
                //let x:Float = 32
                
                //for normaled graph
                //frame.size.height = CGFloat(frequencies[n] * 32 / fftFlasher.fMeans[n])
                
                //for first graph
                frame.size.height = CGFloat(frequencies[n] * fftFlasher.fSliderValues[n] * fftFlasher.graphZoomPow)
                
                frame.origin.y = originY - frame.size.height
                layers[n].frame = frame
                frame.origin.x += width
                //check if would flash and change color to something else
                
                if(frequencies[n] < fftFlasher.fSiMeans[n]*fftFlasher.silenceThreshold){
                    layers[n].backgroundColor = UIColor.init(white: 0.1, alpha: 1).cgColor
                }
                
                else if(fftFlasher.fFlashedOnce[n] ){
                    layers[n].backgroundColor = UIColor.white.cgColor
                }
                else{
                    layers[n].backgroundColor = UIColor.init(white: 0.3, alpha: 1).cgColor
                }
                
                meanFrame.size.height = 2
                silenceFrame.size.height = 2
                
                //for normaled graph
                //meanFrame.origin.y = originY - (CGFloat(  32  ))
                
                //for first graph
                meanFrame.origin.y = originY - (CGFloat(fftFlasher.graphZoomPow * fftFlasher.fMeans[n]))
                meanLayers[n].frame = meanFrame
                meanFrame.origin.x += width
                
                silenceFrame.origin.y = originY - (CGFloat(fftFlasher.graphZoomPow * fftFlasher.fSiMeans[n] * fftFlasher.silenceThreshold))
                silenceLayers[n].frame = silenceFrame
                silenceFrame.origin.x += width
            }
            CATransaction.commit()
        }
        frequencies.deallocate()
    }
    /** end of superpowererd stuff**/
    
}
