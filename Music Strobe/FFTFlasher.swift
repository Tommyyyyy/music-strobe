//
//  Flasher.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 28/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//


import CoreMedia
import CoreAudioKit
import AVFoundation


class FFTFlasher : NSObject {
    
    
    var cameraDevice : AVCaptureDevice?
    
    //sound analysis settings
    ///higher value creates higher threshold for not flashing
    var silenceThreshold : Float  = 3
    
    var graphZoomPow : Float = 32
    
    let fMeans:UnsafeMutablePointer<Float>
    let fTotals:UnsafeMutablePointer<Float>
    let fSiMeans:UnsafeMutablePointer<Float>
    let fSiTotals:UnsafeMutablePointer<Float>
    let fSliderValues:UnsafeMutablePointer<Float>
    let fFlashedOnce:UnsafeMutablePointer<Bool>
    
    var f:Int!
    
    var bufferQueueSize = 7
    var silenceBufferQueueSize = 100
    
    
    
    var fBuffers = [Queue<Float>]()
    var fSilenceBuffers = [Queue<Float>]()
    
    var flashOn = false
    var isRunning = false
    var setupDone = false
    var screenOn = false
    
    
    var isRandomColor = false
    var screenColor = UIColor.white
    
    //TODO this seems pretty shit way to do this
    var ledFlashBtn : UIBarButtonItem
    
    //
    fileprivate  var defaults = UserDefaults.standard
    var flasherView:FlasherView!
    
    // for use in evaluate. keeps track of wether we need to flash on this iteration of n so we can
    //skip checking for flashes for the rest of n
    var flashed = false;
    
    
    init(flasherView:FlasherView ,  numFrequencies:Int ,ledFlashBtn:UIBarButtonItem){
        
        self.cameraDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        
        self.ledFlashBtn = ledFlashBtn
        self.flasherView = flasherView
        self.f = numFrequencies
        fMeans = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fTotals = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fSiMeans = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fSiTotals = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fSliderValues = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fFlashedOnce = UnsafeMutablePointer<Bool>.allocate(capacity: f)
        //temp array to hold
        var tempSliderValues : [NSNumber] = [NSNumber]()
        
        if(defaults.object(forKey: "sliderValues") != nil){
            tempSliderValues =  defaults.object(forKey: "sliderValues") as! [NSNumber]
        }
        else{
            // if no  value has been set then use these values
            tempSliderValues = [0.62,0.62,0.62,0.62,0.62,0.62,0.62,0.62]
        }
        

        for i in 0...f-1 {
 
            
            //should set this as the same in viewcontroller first value
            // fSliderValues[i] = 0.5
            fSliderValues[i] = tempSliderValues[i].floatValue

            //loading falses into array so can check if flashed and only flash once when above mean
            fFlashedOnce[i] = false
            
            
            //load silence values with 100
            //need a high value should be set a lot lower pretty quickly
            //takes a while to go down from 100 so making it lower
            //actaully better to start low and rise up because if music is playing 
            //then it wont register the silence needed to drop the silence threshold
            fSiMeans[i] = 0.2
            fSiTotals[i] = 0
            
            fSilenceBuffers.append(Queue<Float>())
            for _ in 1...silenceBufferQueueSize {
                //load silence buffers aswell
                //starting means and values at 100
                fSilenceBuffers[i].enqueue(0.2)
                fSiTotals[i] = fSiTotals[i] + 0.2
            }
       //     NSLog(" silence size at init " + String( fSilenceBuffers.endIndex))
        
        }
   
        super.init()
    }
    
    
    
    //in seperate method because gets called when changing settings rather than just when initilising
    func setupBuffer(){
        //reset totals to 0 when loading a new queue
        
        fBuffers = [Queue<Float>]()
        for i in 0...f-1 {
            
            fTotals[i] = 0
            fMeans[i] = 0
            
            //adding Queues to each buffer
            fBuffers.append ( Queue<Float>() )
            
            //load buffers with 0's
            for _ in 1...bufferQueueSize {
                fBuffers[i].enqueue(0)
            }
            
        }
    }
    
    func setupTorch(){
        
        do{
            if let cameraDevice = cameraDevice, cameraDevice.hasTorch{
                try cameraDevice.lockForConfiguration()
          //      NSLog(" has torch and is locked")
                
            }
            else{
                //hide the flash button so cant turn on flash
                ledFlashBtn.isEnabled = false
                ledFlashBtn.tintColor = UIColor.clear
                
            }
        }catch{
           // print(error)
        }
    }
    
    var count = 0
    
    
    func evaluate(_ frequencies:UnsafeMutablePointer<Float>){
        
        
        //reversing the slider so a lower value shows higher threshold
        // ranges 1 -> 0.01
        
        
        
        for n in 0...f-1 {
            
            
            frequencies[n] = frequencies[n] * 1024
            
            
            fBuffers[n].enqueue(frequencies[n])
            fTotals[n] += frequencies[n]
            fTotals[n] -= fBuffers[n].dequeue()!
            fMeans[n] = fTotals[n] / Float(bufferQueueSize)
            
            
      
            if(frequencies[n]>0.0001 && frequencies[n]<fSiMeans[n] ){
              
               // NSLog("size:" + String(fSilenceBuffers.endIndex) + " n:" + String(n));
                fSilenceBuffers[n].enqueue(frequencies[n])
                fSiTotals[n] += frequencies[n]
                fSiTotals[n] -= fSilenceBuffers[n].dequeue()!
                fSiMeans[n] = fSiTotals[n] / Float(silenceBufferQueueSize)
                
          //      NSLog(" set freq: " + String(n) + "  " + String(frequencies[n])  + " fsimean: " + String(fSiMeans[n]))
                
            }else{
            
    
            // need a good way of stopping at a good level, 
            //this will rise until its a fraction of the mean
                if(fSiMeans[n] * silenceThreshold < fMeans[n]){
                fSiMeans[n] = (fSiMeans[n] * 1.001)
                }
            
            }
 
            
            
            
            
        /*
            
           
            //redo of silence detection
            if(frequencies[n]>0.001){
                
                // NSLog("size:" + String(fSilenceBuffers.endIndex) + " n:" + String(n));
                fSilenceBuffers[n].enqueue(fMeans[n] / 4)
                
                fSiTotals[n] += fMeans[n] / 4
                fSiTotals[n] -= fSilenceBuffers[n].dequeue()!
                fSiMeans[n] = fSiTotals[n] / Float(silenceBufferQueueSize)
                
             //   NSLog(" set freq: " + String(n) + "  fmean: " + String(fMeans[n] * 0.1)  + " fsimean: " + String(fSiMeans[n]))
                
            }
            
            
            //end redo silence detection
            */
            
            
            frequencies[n] = frequencies[n] * (fSliderValues[n])
            
            
            
            //!Important!!! if the sensethreshold is too low it creates to much flashing it severly slows app and backs up flashes
            
            
            
            
             //slider values range from 0.1 - 1
            //multiply fSiMeans by small amount because we want it to ride higher than the mean
            //
           
            if(frequencies[n] > fMeans[n]   && frequencies[n] > self.fSiMeans[n]*silenceThreshold){
                /**we only want it to flash once when the vol is greater than the mean
                 when vol goes back down lower than mean it can flash again
                 this actually only saves 1 - 3 flashes but may enable the sense theshold
                 to be set higher and no overload the thread with too many flashes
                 **/
                
               // NSLog(" flashed at f:" + String(n), "fuck you");
                if(!fFlashedOnce[n]){
                    // flashed makes sure we only flash 1 time when multiple frequencies want to
                    //flash on the same interation of all teh frequencies
                    //we want to
                    if(!flashed){
                        //flash using a diferent dispatch queue as the sound listener !important
                        self.flash()
                        flashed = true
                    }
                    fFlashedOnce[n] = true
                }
                else{
                    count = count + 1
                    
                }
                
                
            }
            else{
                fFlashedOnce[n] = false
                count = 0
            }
            
            
            
            /*
             self.roLabel.text = ("rms  " + String(frequencies[1]) + "\n mean " + String( self.means[1]) +
             "\n frequency vals" + String(frequencies[1]) + "\n slider" + String(self.fSliderValues[1] )  ) */
            
        }
        // end of iteration of frequencies so reset flashed
        flashed = false
    }
    
    
    
    //TODO: maybe take the difference of mean and current vol and if its big difference make
    //the delay short and if its small difference(ie a slow sound) make it slightly longer
    // removed arguemnts for this so it only needs to call flash once per iteration of n
    // ie iefunc flash(rms:Float,mean:Float){
    //can actaully keep track of differences and use the biggest difference ( or smallest)
    
    func flash(){
        
        //  set the screen flash on first then turn flash on and off then turn screen flash off
        // this ensures that the screen will turn on a visible period.
        
        if(self.screenOn){
            GlobalMainQueue.async{
                if(self.isRandomColor){
                    self.flasherView.changeColor(UIColor.randomColor())
                }
                
                self.flasherView.on()
                
                //have a delay here because it wont be visible as it will flash too fast
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.05 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    self.flasherView.off()
                }
            }
        }
        
        
        if(flashOn){
            GlobalUserInitiatedQueue.async{
                if let cameraDevice = self.cameraDevice {
                    cameraDevice.torchMode = AVCaptureDevice.TorchMode.on
                    cameraDevice.torchMode = AVCaptureDevice.TorchMode.off
                }
                
               
            }
        }
        
    }
    
    
    
    func toggleScreenFlash(){
        
        if screenOn {
            screenOn = false
            flasherView.off()
        }
        else { screenOn = true
        }
    }
    
    
    
    ///session needs to be stopped before running this method
    func loadStoredDefaultValues(){
        //this is main for the buffer queue might screw up if you change the bufferqueuesize while running
        
        //checking if these have values set otherwise dont change them from the original values
        if defaults.integer(forKey: "graphZoom")>0 {
            //want the zoom values to be powers of 2 for faster processing so transalte 1 - 6 to 1 - 64
            let fuck = defaults.integer(forKey: "graphZoom")
            graphZoomPow = pow(2,Float(fuck))
            
        
            //    NSLog("set sens threshold from default" + String(graphZoom))
        }
        if(defaults.integer(forKey: "bufferqueuesize")>0){
            bufferQueueSize = defaults.integer(forKey: "bufferqueuesize")
            //   NSLog("set bufferqueuesize from default" + String(bufferQueueSize))
            
        }
        if(defaults.float(forKey: "silencethreshold")>0){
            silenceThreshold = defaults.float(forKey: "silencethreshold")
            // NSLog("set silencethreshold from defaults" + String(silenceThreshold))
        }
        
    }
    
    
    
    //different dispatch queues copied and pasted. these var names have capital letters?
    var GlobalMainQueue: DispatchQueue {
        return DispatchQueue.main
    }
    
    var GlobalUserInteractiveQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
    }
    
    var GlobalUserInitiatedQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
    }
    
    var GlobalUtilityQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
    }
    
    var GlobalBackgroundQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    }
    
    
    
}
