//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

//  The public Objective-C++ stuff we expose to Swift.

#import <Foundation/Foundation.h>


@interface Superpowered: NSObject

- (void)getFrequencies:(float *)freqs;

@end
