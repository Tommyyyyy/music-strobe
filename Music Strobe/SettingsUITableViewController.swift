//
//  SettingsUITableViewController.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 11/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import Foundation
import UIKit
import CoreMedia
import CoreAudioKit
import AVFoundation


class SettingsUIViewController: UITableViewController  {
    
    //used to save settings
    var defaults = UserDefaults.standard
    //keep original values probabaly better way to do this but cbf
    let graphZoomOriginal:Int = 5
    var graphZoom:Int = 5
    let bufferQueueSizeOriginal:Int = 7
    var bufferQueueSize:Int = 7
    let silenceThresholdOriginal:Float = 5
    var silenceThreshold:Float = 5
    
    
    @IBOutlet weak var sensitivityThresholdLabel: UILabel!
    @IBOutlet weak var bufferSizeLabel: UILabel!
    @IBOutlet weak var silenceThresholdLabel: UILabel!
    
   
    
    @IBOutlet weak var sensitivityThresholdSlider: UISlider!
    @IBOutlet weak var bufferSizeSlider: UISlider!
    @IBOutlet weak var silenceThresholdSlider: UISlider!
    
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    
    
    
    @IBAction func sensitivityThresholdSlider(_ sender: UISlider) {
        graphZoom =  Int(round(sender.value))
        sender.value  = Float(graphZoom)
        sensitivityThresholdLabel.text = String(graphZoom)+"x"
        
    }
    
    @IBAction func bufferSizeSliderUpdate(_ sender: UISlider) {
        bufferQueueSize = Int(sender.value)
        bufferSizeLabel.text = String(bufferQueueSize)
    }
    
    @IBAction func silenceThresholdSliderValueChanged(_ sender: UISlider) {
       
        //want to reverse the direction of the slider so that the high the more sensitive
        //same as graph sliders
        silenceThreshold = roundToPlaces(sender.value ,places:2)
        silenceThresholdLabel.text = String(silenceThreshold)
    }
    
    func getPercentageStringFromDecimal(_ decimalPercent:Float) ->String{
        
        let fmt = NumberFormatter()
        fmt.locale = Locale(identifier: "en_US_POSIX")
        fmt.maximumFractionDigits = 0
        fmt.minimumFractionDigits = 0
        
        let l = fmt.string(from: (decimalPercent * 100) as NSNumber )
        
        //let l =  fmt.string(from: ((decimalPercent) * 100))
        
        return l!+"%"
        
    }
    
    
    func open(url: URL) {
        
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: {
                                            (success) in
                                       //     print("Open \(url): \(success)")
                })
            } else {
              UIApplication.shared.openURL(url)
              //  print("Open \(url): \(success)")
            }
        
    }
   
    //link to superpowered
    @IBAction func superpoweredClick(_ sender: UIButton) {
    //    NSLog("clicked superpowered button")
    
        if let url = URL(string: "http://www.superpowered.com"){
        open(url: url)
        }
        
    }
    
    
    @IBAction func restorePurchases(_ sender: AnyObject) {
        
        //MusicStrobeProducts.store.restorePurchases()
        
    }

    

    @IBAction func contactUs(_ sender: UIButton) {
      //  print(" email should open")
        let email = "support@mediocrefireworks.com"
        let url = URL(string: "mailto:\(email)")
        open(url: url!)
    }
 
    
    @IBAction func resetSettings(_ sender: UIButton) {
        //save settings
        defaults.set(graphZoomOriginal, forKey: "graphZoom")
        defaults.set(bufferQueueSizeOriginal, forKey: "bufferqueuesize")
        defaults.set(silenceThresholdOriginal, forKey: "silencethreshold")
        
        updateSlidersAndLabels()
        
    }
    
    
    func updateSlidersAndLabels(){
        if defaults.integer(forKey: "graphZoom")>0 {
            graphZoom = defaults.integer(forKey: "graphZoom")
            NSLog("got graphZoom from default" + String(graphZoom))
        }
        if(defaults.integer(forKey: "bufferqueuesize")>0){
            bufferQueueSize = defaults.integer(forKey: "bufferqueuesize")
         //   NSLog("got bufferqueuesize from default" + String(bufferQueueSize))
            
        }
        if(defaults.float(forKey: "silencethreshold")>0){
            silenceThreshold = defaults.float(forKey: "silencethreshold")
         //   NSLog("got silencethreshold from defaults" + String(silenceThreshold))
        }
        
        
        //sensThreshold
        sensitivityThresholdSlider.value = Float(graphZoom)
        sensitivityThresholdLabel.text = String(graphZoom) + "x"
        
        //bufferSize
        bufferSizeLabel.text = String(bufferQueueSize)
        bufferSizeSlider.value = Float(bufferQueueSize)
        
        //silenceThreshold
        silenceThresholdSlider.value = silenceThreshold
        silenceThresholdLabel.text = String(silenceThreshold)
        
    }
    
    func roundToPlaces(_ value:Float, places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return round(value * divisor) / divisor
    }
    
    
    
    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion:nil)
    }
    
    
    
    

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = UIColor.black
        
        tableView.allowsSelection = false
        
      //  NSLog(" viewDidLoad updatesliders and labels")
        updateSlidersAndLabels()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if saveBtn === sender as! UIBarButtonItem{
            //save settings
      
            defaults.set(graphZoom, forKey: "graphZoom")
            defaults.set(bufferQueueSize, forKey: "bufferqueuesize")
            defaults.set(silenceThreshold, forKey: "silencethreshold")
       //     NSLog("set settings" + "graph Zoom:" + String(graphZoom)+" bufferqueuesize:" + String(bufferQueueSize))
        }
       // NSLog("preparing for segue")
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
