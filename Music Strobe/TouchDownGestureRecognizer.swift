//
//  TouchDownGestureRecognizer.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 28/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class TouchDownGestureRecognizer: UIGestureRecognizer {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {

     //   NSLog("began")
            self.state = .began

        super.touchesBegan(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        
        self.state = .failed
     
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
   //     NSLog("touches ended")
        self.state = .failed
    }
    
}
