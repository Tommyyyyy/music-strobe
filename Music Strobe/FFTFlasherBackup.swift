//
//  Flasher.swift
//  Music Strobe
//
//  Created by Thomas Kiddle on 28/10/15.
//  Copyright © 2015 Thomas Kiddle. All rights reserved.
//


import CoreMedia
import CoreAudioKit
import AVFoundation


class FFTFlasherBackUp : NSObject {
    
    
    var cameraDevice : AVCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)!
    
    //sound analysis settings
    var silenceThreshold : Float  = 3
    ///higher value creates higher threshold for not flashing
    var sensitivityThreshold : Float = 1.58
    
    let fMeans:UnsafeMutablePointer<Float>
    let fTotals:UnsafeMutablePointer<Float>
    let fSliderValues:UnsafeMutablePointer<Float>
    let fSilenceValues:UnsafeMutablePointer<Float>
    let fFlashedOnce:UnsafeMutablePointer<Bool>
    
    var f:Int!
    
    var bufferQueueSize = 10
    var silenceBufferQueueSize = 100
    var fBuffers = [Queue<Float>]()
    var fSilenceBuffers = [Queue<Float>]()
    
    var flashOn = false
    var isRunning = false
    var setupDone = false
    var screenOn = false
    
    
    var isRandomColor = false
    var screenColor = UIColor.white
    
    //TODO this seems pretty shit way to do this
    var ledFlashBtn : UIBarButtonItem
    
    //
    fileprivate  var defaults = UserDefaults.standard
    var flasherView:FlasherView!
    
    // for use in evaluate. keeps track of wether we need to flash on this iteration of n so we can
    //skip checking for flashes for the rest of n
    var flashed = false;
    
    
    init(flasherView:FlasherView ,  numFrequencies:Int ,ledFlashBtn:UIBarButtonItem){
        self.ledFlashBtn = ledFlashBtn
        self.flasherView = flasherView
        self.f = numFrequencies
        fMeans = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fTotals = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fSliderValues = UnsafeMutablePointer<Float>.allocate(capacity: f)
        fSilenceValues = UnsafeMutablePointer<Float>.allocate(capacity: f);
        fFlashedOnce = UnsafeMutablePointer<Bool>.allocate(capacity: f)
        //temp array to hold
        var fuckup : [NSNumber] = [NSNumber]()
        
        if(defaults.object(forKey: "sliderValues") != nil){
            fuckup =  defaults.object(forKey: "sliderValues") as! [NSNumber]
        }
        else{
            // if no  value has been set then use these values
            fuckup = [0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75]
        }
        
        
        for i in 0...f-1 {
            fTotals[i] = 0
            fMeans[i] = 0
            
            //should set this as the same in viewcontroller first value
            // fSliderValues[i] = 0.5
            fSliderValues[i] = fuckup[i].floatValue
            //load silence values with 0
            fSilenceValues[i] = 0;
            //loading falses into array so can check if flashed and only flash once when above mean
            fFlashedOnce[i] = false
            
        }
        
        
        
        super.init()
    }
    
    
    
    
    func setupBuffer(){
        //reset totals to 0 when loading a new queue
        
        fBuffers = [Queue<Float>]()
        for i in 0...f-1 {
            
            fTotals[i] = 0
            
            fBuffers.append ( Queue<Float>() )
            for _ in 1...bufferQueueSize {
                fBuffers[i].enqueue(0)
            }
        }
    }
    
    func setupTorch(){
        
        do{
            if(cameraDevice.hasTorch){
                try cameraDevice.lockForConfiguration()
          //      NSLog(" has torch and is locked")
            }
            else{
                //hide the flash button so cant turn on flash
                ledFlashBtn.isEnabled = false
                ledFlashBtn.tintColor = UIColor.clear
                
            }
        }catch{
           // print(error)
        }
    }
    
    var count = 0
    
    
    func evaluate(_ frequencies:UnsafeMutablePointer<Float>){
        
        
        //reversing the slider so a lower value shows higher threshold
        // ranges 1 -> 0.01
        
        
        
        for n in 0...self.f-1 {
            
            
            frequencies[n] = frequencies[n] * 1024
            
            self.fBuffers[n].enqueue(frequencies[n])
            
            
            self.fTotals[n] += frequencies[n]
            self.fTotals[n] -= self.fBuffers[n].dequeue()!
            self.fMeans[n] = self.fTotals[n] / Float(self.bufferQueueSize)
            
            //slider values range from 0.1 - 1
            //TODO have majority of this running outside the for loop
            self.fMeans[n] = self.fMeans[n] / self.sensitivityThreshold / fSliderValues[n]
            
            
            
            
            //!Important!!! if the sensethreshold is too low it creates to much flashing it severly slows app and backs up flashes
            
            
            
            
            
            
            if(frequencies[n] > self.fMeans[n] && frequencies[n] > self.silenceThreshold){
                /**we only want it to flash once when the vol is greater than the mean
                 when vol goes back down lower than mean it can flash again
                 this actually only saves 1 - 3 flashes but may enable the sense theshold
                 to be set higher and no overload the thread with too many flashes
                 **/
                if(!fFlashedOnce[n]){
                    // flashed makes sure we only flash 1 time when multiple frequencies want to
                    //flash on the same interation of all teh frequencies
                    //we want to
                    if(!flashed){
                        //flash using a diferent dispatch queue as the sound listener !important
                        self.flash()
                        flashed = true
                    }
                    fFlashedOnce[n] = true
                }
                else{
                    count = count + 1
                  //  print(" didnt flash " + String(count))
                }
                
                
            }
            else{
                fFlashedOnce[n] = false
                count = 0
            }
            
            
            
            /*
             self.roLabel.text = ("rms  " + String(frequencies[1]) + "\n mean " + String( self.means[1]) +
             "\n frequency vals" + String(frequencies[1]) + "\n slider" + String(self.fSliderValues[1] )  ) */
            
        }
        // end of iteration of frequencies so reset flashed
        flashed = false
    }
    
    
    
    //TODO: maybe take the difference of mean and current vol and if its big difference make
    //the delay short and if its small difference(ie a slow sound) make it slightly longer
    // removed arguemnts for this so it only needs to call flash once per iteration of n
    // ie iefunc flash(rms:Float,mean:Float){
    //can actaully keep track of differences and use the biggest difference ( or smallest)
    
    func flash(){
        
        //  set the screen flash on first then turn flash on and off then turn screen flash off
        // this ensures that the screen will turn on a visible period.
        
        if(self.screenOn){
            GlobalMainQueue.async{
                if(self.isRandomColor){
                    self.flasherView.changeColor(UIColor.randomColor())
                }
                
                self.flasherView.on()
                
                //have a delay here because it wont be visible as it will flash too fast
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.05 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    self.flasherView.off()
                }
                
            }
            
        }
        
        
        if(flashOn){
            GlobalUserInitiatedQueue.async{
                self.cameraDevice.torchMode = AVCaptureDevice.TorchMode.on
                self.cameraDevice.torchMode = AVCaptureDevice.TorchMode.off
                //try cameraDevice.setTorchModeOnWithLevel(1.0)
            }
        }
        
    }
    
    
    
    func toggleScreenFlash(){
        
        if screenOn {
            screenOn = false
            flasherView.off()
        }
        else { screenOn = true
        }
    }
    
    
    
    ///session needs to be stopped before running this method
    func loadStoredDefaultValues(){
        //this is main for the buffer queue might screw up if you change the buffergueuesize while running
        
        //checking if these have values set otherwise dont change them from the original values
        if defaults.float(forKey: "sensitivitythreshold").isNormal {
            sensitivityThreshold = defaults.float(forKey: "sensitivitythreshold")
            //    NSLog("set sens threshold from default" + String(sensitivityThreshold))
        }
        if(defaults.integer(forKey: "bufferqueuesize")>0){
            bufferQueueSize = defaults.integer(forKey: "bufferqueuesize")
            //   NSLog("set bufferqueuesize from default" + String(bufferQueueSize))
            
        }
        if(defaults.float(forKey: "silencethreshold")>0){
            silenceThreshold = defaults.float(forKey: "silencethreshold")
            // NSLog("set silencethreshold from defaults" + String(silenceThreshold))
        }
        
    }
    
    
    
    //different dispatch queues copied and pasted. these var names have capital letters?
    var GlobalMainQueue: DispatchQueue {
        return DispatchQueue.main
    }
    
    var GlobalUserInteractiveQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
    }
    
    var GlobalUserInitiatedQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
    }
    
    var GlobalUtilityQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
    }
    
    var GlobalBackgroundQueue: DispatchQueue {
        return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    }
    
    
    
}
